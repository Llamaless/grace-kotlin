# Grace Kotlin

Revamped version of the Grace Church App

Main features: 
- Use of Uri to link out to various social media both via apps and browsers. 
- Use of native functions to allow users to dial numbers from the app. 
- External packages to create pre-built emails in the users prefered email application. 
- Importign an RSS feed to create an automatically updating feed for podcasts
- Media player to play podcast from RSS feed
- Use of the built in google maps api to provide location data/ 
- Use of Mongo DB and Date functions to create a dynamic events feed. 
- Use of SQL to create a local databse for selected podcasts 