import android.annotation.SuppressLint
import android.content.Context
import android.net.ConnectivityManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.llamaless.gracechruchmk.*


class RevisedSermonFragment: Fragment() {

    companion object {
        fun newInstance(): RevisedSermonFragment {
            return RevisedSermonFragment()
        }
    }

    private lateinit var recyclerView: RecyclerView
    private lateinit var titleText: TextView
    private lateinit var swipeRefreshLayout: SwipeRefreshLayout
    private lateinit var viewModel: SermonViewModel
    private lateinit var adapter: RevisedSermonAdapter

    private val isNetworkAvailable: Boolean get() {
        val connectivityManager = context!!.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return  activeNetworkInfo != null && activeNetworkInfo.isConnected
    }

    @SuppressLint("ResourceAsColor")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.sermon_detail, container, false)

        viewModel  = ViewModelProviders.of(this).get(SermonViewModel::class.java)

        recyclerView = view.findViewById(R.id.recycler_sermon_d)
        titleText = view.findViewById(R.id.sermon_title_d)

        titleText.text = getString(R.string.sermons)
        val decoration = DividerItemDecoration(context, 1)
        recyclerView.addItemDecoration(decoration)
        swipeRefreshLayout = view.findViewById(R.id.sermon_swipe_d)

        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.setHasFixedSize(true)

        viewModel.getArticle().observe(this, Observer { articles ->

            if(articles != null){
                adapter = RevisedSermonAdapter(context!!, articles)
                recyclerView.adapter = adapter
                adapter.notifyDataSetChanged()
                swipeRefreshLayout.isRefreshing = false
            }
        })

        swipeRefreshLayout.setColorSchemeColors(R.color.colorPrimaryDark)
        swipeRefreshLayout.canChildScrollUp()
        swipeRefreshLayout.setOnRefreshListener {
            adapter.articles.clear()
            adapter.notifyDataSetChanged()
            swipeRefreshLayout.isRefreshing = true
            viewModel.fetchFeed()
        }

        if (!isNetworkAvailable){
            //////
            val y = 1
            print(y)
        } else if (isNetworkAvailable){
            viewModel.fetchFeed()
        }
        return view
    }


}

