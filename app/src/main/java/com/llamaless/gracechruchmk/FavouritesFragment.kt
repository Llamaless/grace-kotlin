package com.llamaless.gracechruchmk

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout

class FavouritesFragment: Fragment(){


    companion object{
        fun newInstance(): FavouritesFragment{
            return FavouritesFragment()
        }
    }

    private lateinit var recyclerView: RecyclerView
    private lateinit var swipeRefreshLayout: SwipeRefreshLayout
    private lateinit var sermonList: ArrayList<Favourite>

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_favourite, container, false)

        val dbHandler = DBHandler(this.context!!, null, null, 1)

        recyclerView = view.findViewById(R.id.recycler_fav)
        val decoration = DividerItemDecoration(context, 1)
        recyclerView.addItemDecoration(decoration)

        swipeRefreshLayout = view.findViewById(R.id.fav_swipe)

        swipeRefreshLayout.setOnRefreshListener {
            sermonList = dbHandler.getAllFavourites()
            val sortedList = sermonList.sortedBy { Favourite -> Favourite.id }
            val adapter = FavouritesAdapter(context, sortedList)
            recyclerView.adapter = adapter
            recyclerView.layoutManager = LinearLayoutManager(context)
            swipeRefreshLayout.isRefreshing = false
        }


        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val dbHandler = DBHandler(this.context!!, null, null, 1)
        sermonList = dbHandler.getAllFavourites()
        val sortedList = sermonList.sortedBy { Favourite -> Favourite.favouriteName }
        val adapter = FavouritesAdapter(context, sortedList)
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(context)
    }
}