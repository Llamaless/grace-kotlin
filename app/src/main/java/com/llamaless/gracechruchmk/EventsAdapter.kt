package com.llamaless.gracechruchmk

import android.app.Activity
import android.content.Context
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import java.text.ParseException
import java.text.ParsePosition
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*

class EventsAdapter (private var c: Context?, private var list: List<EventsModel>) :
    RecyclerView.Adapter<EventsAdapter.ListViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListViewHolder {
        val view = LayoutInflater.from(c).inflate(R.layout.events_entry, parent, false)
        return ListViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ListViewHolder, position: Int) {

        holder.date.text = list[position].getDate()
        holder.title.text = list[position].getTitle()

        holder.cardView.setOnClickListener {
            val bundle = Bundle()
            bundle.putString("name", list[position].getTitle())
            bundle.putString("fullDate", list[position].getFullDate())
            bundle.putString("info", list[position].getInfo())
            bundle.putString("start", list[position].getStartTime())
            bundle.putString("end", list[position].getEndTime())
            bundle.putInt("month", list[position].getMonth())
            bundle.putInt("day", list[position].getDay())
            bundle.putInt("year", list[position].getYear())
            changeFragment(bundle)
        }
    }


    private fun changeFragment(bundle: Bundle){
        val eventInfoFragment = EventInfoFragment()
        eventInfoFragment.arguments = bundle
        val currentActivity: Activity = (c as MainActivity?)!!
        val transaction =  (currentActivity as MainActivity).supportFragmentManager.beginTransaction()

        transaction.replace(R.id.main_frame_layout, eventInfoFragment)
        transaction.addToBackStack("EventsFragment")
        transaction.commit()
    }

    class ListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        //List view holder sets the Text and image view that is to be added to
        internal var date: TextView = itemView.findViewById(R.id.date_date)
        internal var title: TextView = itemView.findViewById(R.id.event_title)
        internal var cardView: CardView = itemView.findViewById(R.id.cardViewEvents)
    }

}