package com.llamaless.gracechruchmk

import android.app.Activity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.DialogFragment

@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class DeleteDialogue: DialogFragment(){

    private lateinit var name: String

    private lateinit var item: TextView
    private lateinit var no: Button
    private lateinit var yes: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        name = arguments!!.getString("name")
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.dialogue_delete, container, false)

        item = view.findViewById(R.id.delete_item)
        no = view.findViewById(R.id.delete_no)
        yes = view.findViewById(R.id.delete_yes)

        item.text = name

        no.setOnClickListener {
            dialog?.dismiss()
        }

        yes.setOnClickListener {
            val dbHandler = DBHandler(this.context!!, null, null, 1)
            dbHandler.deleteFavourite(name)

            val currentActivity: Activity = (context as MainActivity?)!!
            val transaction =  (currentActivity as MainActivity).supportFragmentManager.beginTransaction()

            transaction.replace(R.id.main_frame_layout, FavouritesFragment())
            transaction.addToBackStack(null)
            transaction.commit()
            dialog?.dismiss()
        }

        return view
    }
}