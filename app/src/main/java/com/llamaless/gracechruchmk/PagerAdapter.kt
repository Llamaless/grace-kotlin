package com.llamaless.gracechruchmk

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter

class PagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm){
    override fun getCount(): Int {
        return 2
    }

    override fun getItem(position: Int): Fragment {
        return when(position){
            0 -> AboutFragment()
            1 -> LocationFragment()
            else -> null
        }!!

    }

    override fun getPageTitle(position: Int): CharSequence? {
        return when(position){
            0 -> "About Us"
            else -> {return "Location"}
        }
    }

}