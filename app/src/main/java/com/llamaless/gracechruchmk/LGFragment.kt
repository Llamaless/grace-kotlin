package com.llamaless.gracechruchmk

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment

class LGFragment: Fragment(){

    private lateinit var safeguarding: Button
    private lateinit var leaflet: Button
    private lateinit var declaration: Button

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_lg, container, false)

        safeguarding = view.findViewById(R.id.lg_ky_safeguarding)
        leaflet = view.findViewById(R.id.lg_gift_aid_leaflet)
        declaration = view.findViewById(R.id.lg_gift_aid_dec)

        safeguarding.setOnClickListener {
            try {
                val i = Intent(Intent.ACTION_VIEW, Uri.parse("https://www.gracechurchmiltonkeynes.org/new-here/safeguarding-children.php"))
                activity?.startActivity(i)
            }catch (e : Exception){
                Log.e("Safeguarding Error", e.toString())
            }
        }

        leaflet.setOnClickListener {
            try {
                val i = Intent(Intent.ACTION_VIEW, Uri.parse("https://www.gracechurchmiltonkeynes.org/giving/gift-aid-it%20leaflet%20feb2016_grace.pdf"))
                activity?.startActivity(i)
            }catch (e : Exception){
                Log.e("Leaflet Error", e.toString())
            }
        }

        declaration.setOnClickListener {
            try {
                val i = Intent(Intent.ACTION_VIEW, Uri.parse("https://www.gracechurchmiltonkeynes.org/giving/declaration.php"))
                activity?.startActivity(i)
            }catch (e : Exception){
                Log.e("Declaration Error", e.toString())
            }
        }

        return view
    }
}