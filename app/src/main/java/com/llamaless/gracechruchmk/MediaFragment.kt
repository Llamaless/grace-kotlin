package com.llamaless.gracechruchmk


import android.annotation.SuppressLint
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.media.AudioManager
import android.media.MediaPlayer
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import java.io.IOException
import java.util.concurrent.TimeUnit


class MediaFragment: Fragment(){
    private lateinit var title: String
    private lateinit var uri: String

    private lateinit var forward: Button
    private lateinit var rewind: Button
    private lateinit var play: Button
    private lateinit var pause: Button
    private lateinit var download: Button
    private lateinit var favouriteYes: Button
    private lateinit var favouriteNo: Button
    private lateinit var  mediaPlayer: MediaPlayer
    private lateinit var notificationManager: NotificationManager

    private var startTime: Double = 0.0
    private var endTime: Double = 0.0

    private var handler: Handler = Handler()
    private var forwardTime: Int = 30000
    private var backTime: Int = 30000
    private lateinit var seekBar: SeekBar
    private lateinit var txt1: TextView
    private lateinit var txt2: TextView
    private lateinit var txt3: TextView

    private var oneTimeOnly: Int = 0
    private var channelId = "com.llamaless.gracechruchmk.media"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        title = arguments?.getString("title")!!
        uri = arguments?.getString("uri")!!
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_media, container, false)

        forward = view.findViewById(R.id.mediaForward)
        play = view.findViewById(R.id.mediaPlay)
        rewind = view.findViewById(R.id.mediaRewind)
        pause = view.findViewById(R.id.mediaPause)
        download = view.findViewById(R.id.mediaDownload)
        favouriteYes = view.findViewById(R.id.mediaFavourite_yes)
        favouriteNo = view.findViewById(R.id.mediaFavourite_no)
        pause.isEnabled = false

        txt1 = view.findViewById(R.id.mediaSmallFirst)
        txt2 = view.findViewById(R.id.mediaSmallSecond)
        txt3 = view.findViewById(R.id.mediaMedium)
        txt3.text = title

        seekBar = view.findViewById(R.id.seekBar)
        seekBar.isClickable = false

        val checked = checkFav()

        if (checked){
            favouriteYes.visibility = View.VISIBLE
            favouriteNo.visibility = View.GONE
        }

        val usedUri = Uri.parse(uri)
        try{
            mediaPlayer = MediaPlayer()
            mediaPlayer.setDataSource(context!!, usedUri)
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC)
            mediaPlayer.prepare()
            endTime = mediaPlayer.duration.toDouble()
            startTime = mediaPlayer.currentPosition.toDouble()

            if (oneTimeOnly == 0){
                seekBar.max = endTime.toInt()
                oneTimeOnly = 1
            }


            val covert1 = String.format("%d:%d",
                TimeUnit.MILLISECONDS.toMinutes(endTime.toLong()),
                TimeUnit.MILLISECONDS.toSeconds(endTime.toLong()) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(endTime.toLong())))

            txt2.text = covert1

            val convert2 = String.format("%d:%d",
                TimeUnit.MILLISECONDS.toMinutes(startTime.toLong()),
                TimeUnit.MILLISECONDS.toSeconds(startTime.toLong()) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(startTime.toLong())))

            txt1.text = convert2

            seekBar.progress = startTime.toInt()
            handler.postDelayed(updateSongTime, 0)

            notificationManager = activity!!.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

            createNotificationChannel(channelId, title, "Currently Playing")

            val focusChangeListener: AudioManager.OnAudioFocusChangeListener = AudioManager.OnAudioFocusChangeListener {}

            play.setOnClickListener {
                val am = activity!!.getSystemService(Context.AUDIO_SERVICE) as AudioManager
                @Suppress("IMPLICIT_CAST_TO_ANY") val result = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    am.requestAudioFocus(focusChangeListener, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN)
                } else {
                    //TODO("VERSION.SDK_INT < O")
                }

                if (result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED){
                    mediaPlayer.start()
                    sendNotification()
                    pause.isEnabled = true
                    play.isEnabled = false
                    play.setBackgroundColor(resources.getColor(R.color.colorPrimaryDark))
                    pause.setBackgroundColor(resources.getColor(R.color.icons))
                }
            }

            pause.setOnClickListener {
                mediaPlayer.pause()
                pause.isEnabled = false
                play.isEnabled = true
                pause.setBackgroundColor(resources.getColor(R.color.colorPrimaryDark))
                play.setBackgroundColor(resources.getColor(R.color.icons))
            }

            forward.setOnClickListener {
                val temp = startTime

                if ((temp + forwardTime) <= endTime){
                    startTime += forwardTime
                    mediaPlayer.seekTo(startTime.toInt())
                } else{
                    Toast.makeText(context,"Cannot jump forward 30 seconds",Toast.LENGTH_SHORT).show()
                }
            }

            rewind.setOnClickListener {
                val temp = startTime

                if ((temp - backTime) > 0){
                    startTime -= backTime
                    mediaPlayer.seekTo(startTime.toInt())
                }else{
                    Toast.makeText(context,"Cannot jump backward 30 seconds",Toast.LENGTH_SHORT).show()
                }

            }

        mediaPlayer.setOnCompletionListener {
            mediaPlayer.reset()
        }


        }catch (e: IOException){
            e.printStackTrace()
        }

        download.setOnClickListener {
            val i = Intent(Intent.ACTION_VIEW, Uri.parse(uri))
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            context?.startActivity(i)

        }

        favouriteNo.setOnClickListener {
            favouriteYes.visibility = View.VISIBLE
            favouriteNo.visibility = View.GONE
            newFav(title, uri)
        }

        favouriteYes.setOnClickListener {
            favouriteYes.visibility = View.GONE
            favouriteNo.visibility = View.VISIBLE
            removeFav(title)
        }

        mediaPlayer.setOnCompletionListener {
            mediaPlayer.seekTo(0)
            pause.isEnabled = false
            play.isEnabled = true
        }



        return  view
    }

    override fun onDestroy() {
        super.onDestroy()
        mediaPlayer.stop()
        mediaPlayer.release()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notificationManager.deleteNotificationChannel(channelId)
        }
    }

    private val updateSongTime: Runnable = Runnable {
        run {
            startTime = mediaPlayer.currentPosition.toDouble()
            val convert3 = String.format("%d:%d",
                TimeUnit.MILLISECONDS.toMinutes(startTime.toLong()),
                TimeUnit.MILLISECONDS.toSeconds(startTime.toLong()) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(startTime.toLong())))
            txt1.text = convert3
            seekBar.progress = startTime.toInt()
            handler.postDelayed(updateSongTime, 100)
        }
    }

    private fun checkFav(): Boolean{
        val dbHandler = DBHandler(this.context!!, null, null, 1)
        val check: Boolean
        val favourite = dbHandler.findFavourite(title)

        check = favourite != null

        return check
    }

    private fun newFav(name: String, uri: String){
        val dbHandler = DBHandler(this.context!!, null, null, 1)
        val favourite = Favourite(name, uri)

        dbHandler.addFavourite(favourite)
    }

    private fun removeFav(name: String){
        val dbHandler = DBHandler(this.context!!, null, null, 1)
        dbHandler.deleteFavourite(name)
    }

    @SuppressLint("InlinedApi")
    private fun createNotificationChannel(id: String, name: String, @Suppress("SameParameterValue") desc: String){

        val importance = NotificationManager.IMPORTANCE_LOW
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(id, name, importance)
            channel.description = desc
            channel.enableVibration(false)
            notificationManager.createNotificationChannel(channel)
        } else {
            //TODO("VERSION.SDK_INT < O")
        }
    }

    private fun sendNotification(){

        val notificationID = 101


        val notification: Notification

        val notificationLayout = RemoteViews("com.llamaless.gracechruchmk", R.layout.notification_small)
        notificationLayout.setTextViewText(R.id.notify_title, title)
        notificationLayout.setTextViewText(R.id.notify_desc, "Currently Playing")



       if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
           notification = Notification.Builder(context, channelId)
               .setChannelId(channelId)
               .setSmallIcon(R.drawable.ic_play_arrow_black_24dp)
               .setCustomContentView(notificationLayout)
               .setColor(resources.getColor(R.color.colorPrimaryDark))
               .setColorized(true)
               .setAutoCancel(true)
               .build()

           notificationManager.notify(notificationID, notification)

        } else {
            //TODO("VERSION.SDK_INT < O")
        }


    }
}

