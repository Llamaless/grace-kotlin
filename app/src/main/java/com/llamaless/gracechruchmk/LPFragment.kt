package com.llamaless.gracechruchmk

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import java.lang.Exception

class LPFragment: Fragment(){

    private lateinit var alphaButton: Button
    private lateinit var  cpaButton: Button

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_lp, container, false)

        alphaButton = view.findViewById(R.id.alpha_button)
        cpaButton = view.findViewById(R.id.cpa_button)

        alphaButton.setOnClickListener {
            val bundle = Bundle()
            bundle.putString("title", "Alpha Testimony")
            bundle.putString("uri", "https://www.gracechurchmiltonkeynes.org/audio/sermons/2018_09_23_Alpha.mp3")
            changeFragment(bundle)
        }

        cpaButton.setOnClickListener {
            try{
                val i = Intent(Intent.ACTION_VIEW, Uri.parse("http://www.cpafrica.org.uk/"))
                activity?.startActivity(i)
            }catch (e : Exception){
                Log.e("CPA Error", e.toString())
            }
        }
        return view
    }

    private fun changeFragment(bundle: Bundle){
        val mediaFragment = MediaFragment()
        mediaFragment.arguments = bundle
        val currentActivity: Activity = (context as MainActivity?)!!
        val transaction =  (currentActivity as MainActivity).supportFragmentManager.beginTransaction()

        transaction.replace(R.id.main_frame_layout, mediaFragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }

}