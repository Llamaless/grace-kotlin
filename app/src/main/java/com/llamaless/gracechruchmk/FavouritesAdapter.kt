package com.llamaless.gracechruchmk

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar


class FavouritesAdapter(private var c: Context?, private var list: List<Favourite>):
    RecyclerView.Adapter<FavouritesAdapter.ListViewHolder>(){


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListViewHolder {
        val view = LayoutInflater.from(c).inflate(R.layout.favourite_entry, parent, false)
        return ListViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ListViewHolder, position: Int) {
        holder.title.text = list[position].favouriteName
        holder.play.setOnClickListener{
            val bundle = Bundle()
            bundle.putString("title", list[position].favouriteName)
            bundle.putString("uri", list[position].favouriteUri)
            changeFragment(bundle)
        }
        holder.delete.setOnClickListener {
            val bundle = Bundle()
            bundle.putString("name", list[position].favouriteName)
            changeFragment2(bundle)
        }
    }


    private fun changeFragment(bundle: Bundle){
        val mediaFragment = MediaFragment()
        mediaFragment.arguments = bundle
        val currentActivity: Activity = (c as MainActivity?)!!
        val transaction =  (currentActivity as MainActivity).supportFragmentManager.beginTransaction()

        transaction.replace(R.id.main_frame_layout, mediaFragment)
        transaction.addToBackStack("FavouritesFragment")
        transaction.commit()
    }

    private fun changeFragment2(bundle: Bundle){
        val delete = DeleteDialogue()
        delete.arguments = bundle
        val currentActivity: Activity = (c as MainActivity?)!!
        val fragManager = (currentActivity as MainActivity).supportFragmentManager
        delete.show(fragManager, "Delete")

    }

    class ListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        //List view holder sets the Text and image view that is to be added to
        internal var title: TextView = itemView.findViewById(R.id.title_favourite)
        internal var play: Button = itemView.findViewById(R.id.button_fav_play)
        internal var delete: Button = itemView.findViewById(R.id.button_fav_delete)
    }
}