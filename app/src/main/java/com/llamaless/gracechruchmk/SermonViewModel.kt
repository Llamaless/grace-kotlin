package com.llamaless.gracechruchmk

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.prof.rssparser.Article
import com.prof.rssparser.Parser
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import java.lang.Exception

class SermonViewModel: ViewModel() {

    private val url = "http://www.gracechurchmiltonkeynes.org/audio/podcast.xml"

    private val viewModelJob = Job()
    private val coroutineScope = CoroutineScope(Dispatchers.Main + viewModelJob)

    private lateinit var articlesListLive: MutableLiveData<MutableList<Article>>

    fun getArticle(): MutableLiveData<MutableList<Article>> {
        if (!::articlesListLive.isInitialized){
            articlesListLive = MutableLiveData()
        }
        return articlesListLive
    }

    fun setArticle(articleList: MutableList<Article>){
        articlesListLive.postValue(articleList)
    }

    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }

    fun fetchFeed(){
        coroutineScope.launch(Dispatchers.Main){
            try {
                val parser = Parser()
                val articleList = parser.getArticles(url)
                setArticle(articleList)
            } catch (e: Exception){
                e.printStackTrace()
                setArticle(mutableListOf())
            }
        }
    }
}