package com.llamaless.gracechruchmk

import android.content.ContentValues.TAG
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.mongodb.stitch.android.core.Stitch
import com.mongodb.stitch.android.services.mongodb.remote.RemoteMongoClient
import org.bson.Document
import java.time.LocalDate

class EventsFragment: Fragment(){

    companion object{
        fun newInstance(): EventsFragment{
            return EventsFragment()
        }
    }

    private lateinit var recyclerView: RecyclerView
    private var eventsList: ArrayList<EventsModel> = ArrayList()
    private lateinit var swipeRefreshLayout: SwipeRefreshLayout

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_events, container, false)

        recyclerView = view.findViewById(R.id.recycler_events)
        val decoration = DividerItemDecoration(context, 1)
        recyclerView.addItemDecoration(decoration)

        swipeRefreshLayout = view.findViewById(R.id.events_swipe)
        swipeRefreshLayout.setColorSchemeColors(resources.getColor(android.R.color.holo_green_dark),
            resources.getColor(android.R.color.holo_red_dark),
            resources.getColor(android.R.color.holo_blue_dark),
            resources.getColor(android.R.color.holo_orange_dark))

        swipeRefreshLayout.setOnRefreshListener {
            dbFillList(object : EventsCallback{
                override fun onCallback(value: ArrayList<EventsModel>) {
                    eventsList = value
                    val sortedList = eventsList.sortedWith(compareBy ({it.getYear()}, {it.getMonth()}, {it.getDay()}) )
                    val adapter = EventsAdapter(context, sortedList)
                    recyclerView.adapter = adapter
                    recyclerView.layoutManager = LinearLayoutManager(context)
                }
            })
            swipeRefreshLayout.isRefreshing = false
        }
        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        dbFillList(object : EventsCallback{
            override fun onCallback(value: ArrayList<EventsModel>) {
                eventsList = value
                val sortedList = eventsList.sortedWith(compareBy ({it.getYear()}, {it.getMonth()}, {it.getDay()}, {it.getTitle()}) )
                val adapter = EventsAdapter(context, sortedList)
                recyclerView.adapter = adapter
                recyclerView.layoutManager = LinearLayoutManager(context)
            }
        })
    }

    private fun dbFillList(myCallback: EventsCallback){
        val list = ArrayList<EventsModel>()
        val stitchAppClient = Stitch.getDefaultAppClient()
        val mongoClient = stitchAppClient.getServiceClient(
            RemoteMongoClient.factory,
            "mongodb-atlas"
        )
        val myCollection = mongoClient.getDatabase("GC")
            .getCollection("Events")
        val query = myCollection.find()
        val result = mutableListOf<Document>()

        query.into(result).addOnSuccessListener {
            result.forEach{
                val eventsModel = EventsModel()
                eventsModel.setTitle(it["title"] as String)
                eventsModel.setDate(it["date"] as String)
                eventsModel.setInfo(it["info"] as String)
                eventsModel.setFullDate(it["fullDate"] as String)
                eventsModel.setStartTime(it["start"] as String)
                eventsModel.setEndTime(it["end"] as String)
                eventsModel.setMonth(it["month"] as Int)
                eventsModel.setDay(it["day"] as Int)
                eventsModel.setYear(it["year"] as Int)
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
                    val date = LocalDate.now()
                    val eventsDate = LocalDate.of(eventsModel.getYear(), eventsModel.getMonth(), eventsModel.getDay())

                    if (date.isBefore(eventsDate)){
                        list.add(eventsModel)
                    }
                }else {
                    list.add(eventsModel)
                }
            }
            myCallback.onCallback(list)
        }.addOnFailureListener {
            val exception = it.toString()
            Log.e(TAG, "Error getting documents\n$exception")
        }
    }

    interface EventsCallback{
        fun onCallback(value: ArrayList<EventsModel>)
    }
}