package com.llamaless.gracechruchmk

class Favourite {

    var id: Int = 0
    var favouriteName: String? = null
    var favouriteUri: String? = null
    var favouritePreacher: String? = null

    constructor(id: Int, favouriteName: String, favouriteUri: String){
        this.id = id
        this.favouriteName = favouriteName
        this.favouriteUri = favouriteUri
    }

    constructor(favouriteName: String, favouriteUri: String){
        this.favouriteName = favouriteName
        this.favouriteUri = favouriteUri
    }


}