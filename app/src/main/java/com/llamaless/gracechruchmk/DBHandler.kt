package com.llamaless.gracechruchmk

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper


class DBHandler(context: Context, name: String?, factory: SQLiteDatabase.CursorFactory?,
                version:Int) : SQLiteOpenHelper(context, DATABASE_NAME, factory, DATABASE_VERSION){

    override fun onCreate(db: SQLiteDatabase?) {

        val create = ("CREATE TABLE $TABLE_FAVOURITE($COLUMN_ID INTEGER PRIMARY KEY," +
                "$COLUMN_FAVOURITENAME TEXT,$COLUMN_FAVOURITEURI TEXT)")

        db?.execSQL(create)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db?.execSQL("DROP TABLE IF EXISTS $TABLE_FAVOURITE")
    }

    fun addFavourite(favourite: Favourite){

        val values = ContentValues()
        values.put(COLUMN_FAVOURITENAME, favourite.favouriteName)
        values.put(COLUMN_FAVOURITEURI, favourite.favouriteUri)

        val db = this.writableDatabase

        db.insert(TABLE_FAVOURITE, null, values)
        db.close()
    }

    fun getAllFavourites(): ArrayList<Favourite>{

        val query = "SELECT * FROM $TABLE_FAVOURITE"
        val db = this.readableDatabase
        val cursor = db.rawQuery(query, null)
        val favouriteList = ArrayList<Favourite>()

        if (cursor.moveToFirst()){
            do {
               val id = Integer.parseInt(cursor.getString(0))
               val name = cursor.getString(1)
               val uri = cursor.getString(2)
               val favourite = Favourite(id, name, uri)

               favouriteList.add(favourite)
            }while (cursor.moveToNext())
        }

        cursor.close()
        db.close()
        return favouriteList
    }

    fun findFavourite(favouriteName: String): Favourite?{

        val query = "SELECT * FROM $TABLE_FAVOURITE WHERE $COLUMN_FAVOURITENAME = \"$favouriteName\""
        val db = this.writableDatabase
        val cursor = db.rawQuery(query, null)
        var favourite: Favourite?= null

        if(cursor.moveToFirst()){
            cursor.moveToFirst()
            val id = Integer.parseInt(cursor.getString(0))
            val name = cursor.getString(1)
            val uri = cursor.getString(2)
            favourite = Favourite(id, name, uri)
            cursor.close()
        }

        db.close()
        return favourite
    }

    fun deleteFavourite(favouriteName: String): Boolean{

        var result = false
        val query = "SELECT * FROM $TABLE_FAVOURITE WHERE $COLUMN_FAVOURITENAME = \"$favouriteName\""
        val db = this.writableDatabase
        val cursor = db.rawQuery(query, null)

        if (cursor.moveToFirst()){
            val id = Integer.parseInt(cursor.getString(0))
            db.delete(TABLE_FAVOURITE, "$COLUMN_ID = ?", arrayOf(id.toString()))
            cursor.close()
            result = true
        }
        db.close()
        return result

    }

    fun clearFavourite(): Boolean{

        var result = false
        val query = "SELECT * FROM $TABLE_FAVOURITE"
        val db = this.writableDatabase
        val cursor = db.rawQuery(query, null)

        if(cursor.moveToFirst()){
            do{
                val id = Integer.parseInt(cursor.getString(0))
                db.delete(TABLE_FAVOURITE, "$COLUMN_ID = ?", arrayOf(id.toString()))
            }while (cursor.moveToNext())
            result = true
        }
        cursor.close()
        db.close()
        return result
    }

    companion object{

        private const val DATABASE_VERSION = 1
        private const val DATABASE_NAME = "favouriteDB.db"
        const val TABLE_FAVOURITE = "favourite"

        const val COLUMN_ID = "_id"
        const val COLUMN_FAVOURITENAME = "favouriteName"
        const val COLUMN_FAVOURITEURI = "favouriteUri"
    }
}

