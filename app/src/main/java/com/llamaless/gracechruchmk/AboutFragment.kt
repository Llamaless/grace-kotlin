package com.llamaless.gracechruchmk

import android.app.Activity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment

class AboutFragment: Fragment(){

    private lateinit var aboutGod: Button
    private lateinit var aboutLife: Button
    private lateinit var aboutPeople: Button

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_about, container, false)

        aboutGod = view.findViewById(R.id.about_god)
        aboutLife = view.findViewById(R.id.about_life)
        aboutPeople = view.findViewById(R.id.about_people)

        aboutGod.setOnClickListener {
            changeFragment(LGFragment())
        }

        aboutPeople.setOnClickListener {
            changeFragment(LPFragment())
        }

        aboutLife.setOnClickListener {
            changeFragment(LLFragment())
        }

        return view
    }

    private fun changeFragment(fragment: Fragment){
        val currentActivity: Activity = (context as MainActivity?)!!
        val transaction =  (currentActivity as MainActivity).supportFragmentManager.beginTransaction()

        transaction.replace(R.id.main_frame_layout, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }
}

