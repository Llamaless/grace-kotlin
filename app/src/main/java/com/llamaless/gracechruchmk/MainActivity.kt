package com.llamaless.gracechruchmk

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import com.google.android.material.bottomnavigation.BottomNavigationView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.google.android.material.bottomnavigation.LabelVisibilityMode
import com.mongodb.stitch.android.core.Stitch
import com.mongodb.stitch.core.auth.providers.anonymous.AnonymousCredential

class MainActivity : AppCompatActivity() {

    private lateinit var toolbar: Toolbar
    private lateinit var navView: BottomNavigationView

    private val onNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_home -> {
                val fragment = HomeFragment.newInstance()
                changeFragment(fragment)
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_sermon -> {
                val fragment = RevisedSermonFragment.newInstance()
                changeFragment(fragment)
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_events -> {
                val fragment = EventsFragment.newInstance()
                changeFragment(fragment)
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_contact ->{
                val fragment = ContactFragment.newInstance()
                changeFragment(fragment)
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    private fun changeFragment(fragment: Fragment){
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.main_frame_layout, fragment)
        supportFragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)
        transaction.addToBackStack("main")
        transaction.commit()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        navView = findViewById(R.id.nav_view)
        navView.labelVisibilityMode = LabelVisibilityMode.LABEL_VISIBILITY_LABELED
        navView.setOnNavigationItemSelectedListener(onNavigationItemSelectedListener)

        Stitch.initializeDefaultAppClient(
            resources.getString(R.string.my_app_id)
        )

        val stitchAppClient = Stitch.getDefaultAppClient()

        stitchAppClient.auth.loginWithCredential(AnonymousCredential()).addOnSuccessListener {
            Log.e("TAG", "Successful Mongo Login")
        }.addOnFailureListener {
            Log.e("TAG", "Successful Mongo Failed")
        }

        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.main_frame_layout, HomeFragment.newInstance())
        transaction.addToBackStack(null)
        transaction.commit()

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.toolbar_menu, menu)
        return  true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

        when(item?.itemId){
            R.id.action_settings -> {
                val intent = Intent(this, SettingsActivity::class.java)
                startActivity(intent)
            }
            R.id.action_favourites -> {
                changeFragment(FavouritesFragment.newInstance())
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {

        val count= supportFragmentManager.backStackEntryCount

        if(count == 1){
            navView.menu.getItem(0).isChecked = true
            val transaction = supportFragmentManager.beginTransaction()
            transaction.replace(R.id.main_frame_layout, HomeFragment.newInstance())
            transaction.addToBackStack(null)
            transaction.commit()
        }else{
            super.onBackPressed()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        supportFragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)
    }

    //TODO Write tests for the databases and outlier functions
}
