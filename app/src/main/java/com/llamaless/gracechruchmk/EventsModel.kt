package com.llamaless.gracechruchmk

class EventsModel {
    private var title: String? = null
    private var date: String? = null
    private var fullDate: String? = null
    private var info: String? = null
    private var startTime: String? = null
    private var endTime: String? = null
    private var month: Int = 0
    private var day: Int = 0
    private var year: Int = 0

    fun EventsModel(){}

    fun EventsModel(title: String, date: String, info: String, fullDate: String,
                    startTime: String, endTime: String, month: Int, day: Int, year: Int){
        this.title = title
        this.date = date
        this.info = info
        this.fullDate = fullDate
        this.startTime = startTime
        this.endTime = endTime
        this.month = month
        this.day = day
        this.year = year
    }

    fun getTitle(): String{
        return title.toString()
    }

    fun setTitle(title: String){
        this.title = title
    }

    fun getDate(): String{
        return date.toString()
    }

    fun setDate(date: String){
        this.date = date
    }

    fun getInfo(): String{
        return info.toString()
    }

    fun setInfo(info: String){
        this.info = info
    }

    fun getFullDate(): String{
        return fullDate.toString()
    }

    fun setFullDate(fullDate: String){
        this.fullDate = fullDate
    }

    fun getStartTime(): String{
        return startTime.toString()
    }

    fun setStartTime(startTime: String){
        this.startTime = startTime
    }

    fun getEndTime():String{
        return endTime.toString()
    }

    fun setEndTime(endTime: String){
        this.endTime = endTime
    }

    fun getMonth(): Int{
        return month
    }

    fun setMonth(month: Int){
        this.month = month
    }

    fun getDay():Int{
        return day
    }

    fun setDay(day: Int){
        this.day = day
    }

    fun getYear():Int{
        return year
    }

    fun setYear(year: Int){
        this.year = year
    }
}