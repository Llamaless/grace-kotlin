package com.llamaless.gracechruchmk

import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.content.pm.ResolveInfo
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat
import de.cketti.mailto.EmailIntentBuilder
import kotlinx.android.synthetic.main.activity_main.*

class SettingsActivity : AppCompatActivity() {

    private lateinit var toolbar: Toolbar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.settings_activity)

        toolbar = findViewById(R.id.settings_toolbar)
        setSupportActionBar(toolbar)

        supportFragmentManager
            .beginTransaction()
            .replace(R.id.settings, SettingsFragment())
            .commit()
    }

    class SettingsFragment : PreferenceFragmentCompat() {
        override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
            setPreferencesFromResource(R.xml.root_preferences, rootKey)

            val feedback: Preference? =  findPreference("feedback")
            val info: Preference? = findPreference("info")
            val clear: Preference? = findPreference("clear")

            feedback?.setOnPreferenceClickListener {
                changeFragment(SettingsFeedbackFragment())
                true
            }

            info?.setOnPreferenceClickListener {
                var i : Intent
                i = Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/dev?id=5902701376248610366"))
                val activities: List<ResolveInfo> = activity?.packageManager?.queryIntentActivities(
                    i, PackageManager.MATCH_DEFAULT_ONLY
                )!!
                val isIntentSafe: Boolean = activities.isNotEmpty()
                if (isIntentSafe){
                    activity?.startActivity(i)
                } else{
                    i = Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/dev?id=5902701376248610366"))
                    activity?.startActivity(i)
                }
                true
            }

            clear?.setOnPreferenceClickListener {
                val db = DBHandler(this.context!!, null, null, 1)
                db.clearFavourite()
                true
            }

        }

        private fun changeFragment(fragment: Fragment){
            val currentActivity: Activity = (context as SettingsActivity?)!!
            val transaction =  (currentActivity as SettingsActivity).supportFragmentManager.beginTransaction()

            transaction.replace(R.id.settings, fragment)
            transaction.addToBackStack(null)
            transaction.commit()
        }
    }

    class SettingsFeedbackFragment: Fragment(){
        override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
            val view = inflater.inflate(R.layout.fragment_settings_feedback, container, false)

            val email:Button = view.findViewById(R.id.s_contact_email)
            val subject: TextView = view.findViewById(R.id.s_contact_subject)
            val message: TextView =  view.findViewById(R.id.s_contact_message)

            email.setOnClickListener {
                val subjects = subject.text.toString()
                val body = message.text.toString()
                if (body == ""){
                    Toast.makeText(context, "Please enter a message", Toast.LENGTH_SHORT).show()
                }else{
                    EmailIntentBuilder.from(this.context!!)
                        .to("lldissues@gmail.com")
                        .subject(subjects)
                        .body(body)
                        .start()
                }
            }
            return view
        }
    }

}