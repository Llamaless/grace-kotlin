package com.llamaless.gracechruchmk

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.provider.CalendarContract
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.Fragment
import java.text.SimpleDateFormat
import java.util.*

@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class EventInfoFragment: Fragment() {
    private lateinit var name: String
    private lateinit var fullDate: String
    private lateinit var info: String
    private lateinit var start: String
    private lateinit var end: String
    private var day: Int = 0
    private var month: Int = 0
    private var year: Int = 0
    private lateinit var infoName: TextView
    private lateinit var infoFullDate: TextView
    private lateinit var infoInfo: TextView
    private lateinit var calenderAdd: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        name = arguments!!.getString("name")
        fullDate = arguments!!.getString("fullDate")
        info = arguments!!.getString("info")
        start = arguments!!.getString("start")
        end = arguments!!.getString("end")
        day = arguments!!.getInt("day")
        month = arguments!!.getInt("month")
        year = arguments!!.getInt("year")

    }

    @SuppressLint("SimpleDateFormat")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.events_detail, container, false)

        infoName = view.findViewById(R.id.eventInfo_title)
        infoFullDate = view.findViewById(R.id.eventInfo_date)
        infoInfo = view.findViewById(R.id.eventInfo_info)

        infoName.text = name
        infoFullDate.text = fullDate
        infoInfo.text = info

        calenderAdd = view.findViewById(R.id.eventInfo_add_calendar)

        calenderAdd.setOnClickListener {
            val intent = Intent(Intent.ACTION_INSERT)
            intent.type = "vnd.android.cursor.item/event"
            intent.putExtra(CalendarContract.Events.TITLE, name)
            intent.putExtra(CalendarContract.Events.DESCRIPTION, info)
            val calDateStart = GregorianCalendar(year, month-1, day)
            val calDateEnd = GregorianCalendar(year, month-1, day)
            val sdf = SimpleDateFormat("kk:mm")
            val d1: Date = sdf.parse(start)
            val d2: Date = sdf.parse(end)
            calDateStart.set(Calendar.HOUR_OF_DAY, d1.hours)
            calDateEnd.set(Calendar.HOUR_OF_DAY, d2.hours)
            intent.putExtra(CalendarContract.EXTRA_EVENT_ALL_DAY, false)
            intent.putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, calDateStart.timeInMillis)
            intent.putExtra(CalendarContract.EXTRA_EVENT_END_TIME, calDateEnd.timeInMillis)
            startActivity(intent)
        }

        return view
    }

}