package com.llamaless.gracechruchmk

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment

class LocationFragment: Fragment(){

    private lateinit var locButton: Button

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_location, container, false)

        locButton = view.findViewById(R.id.location_button)

        locButton.setOnClickListener {
            val locIntent: Uri = Uri.parse("geo:0,0?q=Chrysalis Theatre, Japonica Lane, Willen Park South, MK15 9YJ")
            val mapIntent = Intent(Intent.ACTION_VIEW, locIntent)
            mapIntent.setPackage("com.google.android.apps.maps")
            startActivity(mapIntent)

        }

        return view
    }
}