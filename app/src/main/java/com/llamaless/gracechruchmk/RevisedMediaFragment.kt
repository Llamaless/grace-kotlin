package com.llamaless.gracechruchmk

import android.app.NotificationManager
import android.media.MediaPlayer
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.SeekBar
import android.widget.TextView
import androidx.fragment.app.Fragment

class RevisedMediaFragment: Fragment() {

    private lateinit var title: String
    private lateinit var uri: String

    private lateinit var forward: Button
    private lateinit var rewind: Button
    private lateinit var play: Button
    private lateinit var pause: Button
    private lateinit var download: Button
    private lateinit var favouriteYes: Button
    private lateinit var favouriteNo: Button
    private lateinit var  mediaPlayer: MediaPlayer
    private var pauseV:Boolean = false
    private lateinit var notificationManager: NotificationManager

    private var startTime: Double = 0.0
    private var endTime: Double = 0.0

    private var handler: Handler = Handler()
    private var forwardTime: Int = 30000
    private var backTime: Int = 30000
    private lateinit var seekBar: SeekBar
    private lateinit var txt1: TextView
    private lateinit var txt2: TextView
    private lateinit var txt3: TextView

    private var oneTimeOnly: Int = 0
    private var channelId = "com.llamaless.gracechruchmk.media"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        title = arguments?.getString("title")!!
        uri = arguments?.getString("uri")!!
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_media, container, false)

        forward = view.findViewById(R.id.mediaForward)
        play = view.findViewById(R.id.mediaPlay)
        rewind = view.findViewById(R.id.mediaRewind)
        pause = view.findViewById(R.id.mediaPause)
        download = view.findViewById(R.id.mediaDownload)
        favouriteYes = view.findViewById(R.id.mediaFavourite_yes)
        favouriteNo = view.findViewById(R.id.mediaFavourite_no)
        pause.isEnabled = false

        txt1 = view.findViewById(R.id.mediaSmallFirst)
        txt2 = view.findViewById(R.id.mediaSmallSecond)
        txt3 = view.findViewById(R.id.mediaMedium)
        txt3.text = title

        seekBar = view.findViewById(R.id.seekBar)

        val usedUri = Uri.parse(uri)


        return view
    }
}