package com.llamaless.gracechruchmk

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.prof.rssparser.Article
import kotlinx.android.synthetic.main.sermon_detail_entry.view.*
import java.text.SimpleDateFormat
import java.util.*

class RevisedSermonAdapter(val context: Context, val articles: MutableList<Article>): RecyclerView.Adapter<RevisedSermonAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.sermon_detail_entry, parent, false))
    }

    override fun getItemCount(): Int {
        return articles.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(articles[position])

    inner class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){

        fun bind(article: Article){

            val pattern = "dd-MMM-yyyy"
            val sdf = SimpleDateFormat(pattern, Locale.UK)
            val date = sdf.format(Date(article.pubDate))

            itemView.title_sermon.text = article.title

            itemView.preacher_sermon.text = context.getString(R.string.gcmk)

            itemView.date_sermon.text = date

            itemView.setOnClickListener {
                val bundle = Bundle()
                bundle.putString("title", article.title)
                bundle.putString("uri", article.link)
                bundle.putString("preacher", article.author)
                changeFragment(bundle)
            }


        }

    }

    private fun changeFragment(bundle: Bundle){
        val mediaFragment = MediaFragment()
        mediaFragment.arguments = bundle
        val currentActivity: Activity = (context as MainActivity?)!!
        val transaction =  (currentActivity as MainActivity).supportFragmentManager.beginTransaction()

        transaction.replace(R.id.main_frame_layout, mediaFragment)
        transaction.addToBackStack("RevisedSermonFragment")
        transaction.commit()
    }
}