package com.llamaless.gracechruchmk

import android.content.Intent
import android.content.pm.PackageManager
import android.content.pm.ResolveInfo
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.fragment.app.Fragment
import de.cketti.mailto.EmailIntentBuilder

class ContactFragment: Fragment(){
    private lateinit var facebook: Button
    private lateinit var twitter: Button
    private lateinit var phone: Button
    private lateinit var email: Button
    private lateinit var name: EditText
    private lateinit var subject: EditText
    private lateinit var message: EditText

    companion object{
        fun newInstance(): ContactFragment{
            return ContactFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_contact, container, false)

        facebook = view.findViewById(R.id.contact_facebook)
        twitter = view.findViewById(R.id.contact_twitter)
        phone = view.findViewById(R.id.contact_blog)
        email = view.findViewById(R.id.contact_email)
        name = view.findViewById(R.id.contact_name)
        subject = view.findViewById(R.id.contact_subject)
        message = view.findViewById(R.id.contact_message)

        facebook.setOnClickListener {
            var i : Intent
            i = Intent(Intent.ACTION_VIEW, Uri.parse("fb://facewebmodal/f?href=https://www.facebook.com/GraceChurchMiltonKeynes/"))
            val activities: List<ResolveInfo> = activity?.packageManager?.queryIntentActivities(
                i, PackageManager.MATCH_DEFAULT_ONLY
            )!!
            val isIntentSafe: Boolean = activities.isNotEmpty()
            if (isIntentSafe){
                activity?.startActivity(i)
            } else{
                i = Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/GraceChurchMiltonKeynes/"))
                activity?.startActivity(i)
            }

        }

        twitter.setOnClickListener {
            var i : Intent
            i = Intent(Intent.ACTION_VIEW, Uri.parse("twitter://user?screen_name=GraceChurchMK"))
            val activities: List<ResolveInfo> = activity?.packageManager?.queryIntentActivities(
                i, PackageManager.MATCH_DEFAULT_ONLY
            )!!
            val isIntentSafe: Boolean = activities.isNotEmpty()
            if (isIntentSafe){
                activity?.startActivity(i)
            } else{
                i = Intent(Intent.ACTION_VIEW, Uri.parse("https://twitter.com/GraceChurchMK"))
                activity?.startActivity(i)
            }

        }

        phone.setOnClickListener {
            val i = Intent(
                Intent.ACTION_DIAL,
                Uri.parse("tel:01908281380")
            )
            val activities: List<ResolveInfo> = activity?.packageManager?.queryIntentActivities(
                i, PackageManager.MATCH_DEFAULT_ONLY
            )!!
            val isIntentSafe: Boolean = activities.isNotEmpty()
            if (isIntentSafe){
                activity?.startActivity(i)
            }
        }

        email.setOnClickListener {
            val names = name.text.toString()
            val subjects = subject.text.toString()
            val body = message.text.toString()
            if (names == "" || body == ""){
                Toast.makeText(context, "Please enter a name and a message", Toast.LENGTH_SHORT).show()
            }else{
                EmailIntentBuilder.from(this.context!!)
                    .to("office@gracechurchmiltonkeynes.org")
                    .subject("$subjects - $names")
                    .body(body)
                    .start()
            }
        }
        return view
    }
}